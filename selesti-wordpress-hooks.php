<?php
/**
 * Plugin Name: Selesti Helper
 * Plugin URI: https://bitbucket.org/selesti/wordpress-hooks
 * Description: An autoloader that enables several utilities for selesti.
 * Version: 1.0.1
 * Author: Selesti Ltd
 * Author URI: https://www.selesti.com/
 * License: Private
 */

if (!is_blog_installed()) { return; }

if(!function_exists('dd')){
    function dd()
    {
        $args = func_get_args();
        if(sizeof($args > 0))
        {
            foreach ($args as $arg)
            {
                var_dump($arg);
            }
        }
        exit();
    }
}

if(!function_exists('kd')){
    function kd()
    {
        $args = func_get_args();
        if(sizeof($args > 0))
        {
            foreach ($args as $arg)
            {
                var_dump($arg);
            }
        }
    }
}

if(!function_exists('render_view')){
    function render_view( $template , $vars = array() )
    {
        $template_path = get_template_directory() . '/' . ltrim(str_replace('.php', '', $template), '/') . '.php';

        if( !file_exists( $template_path ) ){
            throw new Exception('Template file not found - '. $template_path);
        }

        extract($vars);
        include $template_path;
    }
}

if(!function_exists('fetch_view')){
    function fetch_view( $template , $vars = array() )
    {
        ob_start();
        render_view($template, $vars);
        $html = ob_get_clean();
        return $html;
    }
}

if(!function_exists('angular_json')){
    function angular_json( $data )
    {
        return htmlspecialchars( json_encode( $data ) , ENT_QUOTES, 'UTF-8');
    }
}

if(!function_exists('acf_repeater_by_post_id')){
    function acf_repeater_by_post_id( $repeater_name, $postid )
    {
        global $wpdb;

        $results = $wpdb->get_results( $wpdb->prepare(
            "SELECT * FROM $wpdb->postmeta WHERE meta_key LIKE %s AND post_id = %s",
            $repeater_name.'%_',
            $postid
        ));

        return $results;
    }
}
//acf_repeater_by_post_id( 'consultation_costs', $standard_id );

if(!function_exists('acf_repeater_find_where')){
    function acf_repeater_find_where( $repeater_name, $field, $query )
    {
        global $wpdb;

        $results = $wpdb->get_results( $wpdb->prepare(
            "SELECT * FROM $wpdb->options WHERE option_name LIKE %s",
            'options_'.$repeater_name.'%_'
        ));

        $options = array();

        foreach($results as $r){

            preg_match('_([0-9]+)_', $r->option_name, $matches);
            $row_id = $matches[0];

            if( !isset( $options[$row_id] ) ){
                $options[$row_id] = (object) array();
            }

            $key_name = str_replace(array('options_'.$repeater_name.'_', $row_id.'_'), '', $r->option_name);

            $options[$row_id]->{$key_name} = $r->option_value;
        }

        $found = array_filter($options, function($o) use ($field, $query){
            return $o->{$field} == $query;
        });

        $clean_indexes = array_values($found);
        return array_shift($clean_indexes);
    }
}

if(!function_exists('input_get')){
    function input_get($key, $default = '', $xss_clean = false)
    {
        //First check in the key for it.
        $results = isset( $_GET[ $key ] ) ? $_GET[ $key ] : $default;

        //If its not there, maybe then its in the post?
        if( $results == $default ){
            $results = isset( $_POST[ $key ] ) ? $_POST[ $key ] : $default;
        }

        //If its not there, maybe then its in the post?
        if( $results == $default ){
            $results = isset( $_FILES[ $key ] ) ? $_FILES[ $key ] : $default;
        }

        /* lets do some encoding..maybe..probably shouldnt! */
        if( is_array( $results ) ) foreach($results as &$r){
            is_string( $r ) && $r = mb_convert_encoding( trim($r) , 'UTF-8');
        }
        elseif ( is_string( $results ) ){
            $results = mb_convert_encoding( trim($results) , 'UTF-8');
        }

        if( $xss_clean ){

            if (get_magic_quotes_gpc()) {
                $results = stripslashes($results);
            }

            $search = array(
                '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
                '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
                '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
                '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
            );

            $results = preg_replace($search, '', $results);
            $results = htmlspecialchars(addslashes($results), ENT_QUOTES, 'UTF-8');
        }

        return $results;
    }
}

if(!function_exists('str_contains')){
    function str_contains($haystack, $needle)
    {
        return strpos($haystack, $needle) !== false;
    }
}

if(!function_exists('starts_with')){
    function starts_with($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }
}

if(!function_exists('ends_with')){
    function ends_with($haystack, $needle)
    {
        $length = strlen($needle);

        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }
}


/**
* Selesti SSO Section
**/

function selesti_sso_markup() {?>

    <!-- Google SSO -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="<?php echo GOOGLE_SSO_CLIENT_ID ?>">
    <style>
        #google-sso-login {
            display: none;
            padding-bottom: 100px;
        }
        #google-sso-login .abcRioButton {
            width: 180px !important;
            margin: 0 auto;
            transform: translateY(20px);
        }
        #google-sso-login .abcRioButtonContents span:before {
            content: "Selesti SSO "
        }
    </style>

    <script type="text/javascript">

        if(typeof jQuery == 'undefined'){
            document.write('<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></'+'script>');
        }

        var googleUser = null;

        var onSsoSuccess = function(_gU){

            if(!googleUser){
                googleUser = _gU;
            }

            if( !jQuery('#google-sso-login').data('clicked') ){
                return false;
            }

            var sso_token = googleUser.getAuthResponse().id_token;

            jQuery.post('/wp/wp-admin/admin-ajax.php?action=selesti_sso_login', {
                sso_token: sso_token
            },function(r){

                try {
                    if(r.success){
                        window.location = '/wp/wp-admin';
                    }
                    else {

                        if( r.exception ){
                            googleUser.disconnect();
                        }

                        alert(r.message);
                    }
                } catch(e){
                    alert("Sorry something went wrong, please check the console and report it.")
                    console.log(e);
                }
            });
        };

        var markClicked = function(){
            jQuery('#google-sso-login').data('clicked', true);
        };

        var seq = [83,83,79],
            marker = 0,
            matchPattern = function(e){

                if( e.keyCode == seq[marker] ){

                    marker++;

                    if( marker == seq.length ){
                        window.removeEventListener('keydown', matchPattern);
                        document.getElementById('google-sso-login').style.display = 'block';

                        if( googleUser ){
                            jQuery('#google-sso-login').data('clicked', true)
                            onSsoSuccess(googleUser);
                        }
                        return true;
                    }
                }
                else {
                    marker = 0;
                }
            };

        window.addEventListener('keydown', matchPattern);

    </script>
    <!-- End Google SSO -->

<?php }

function selesti_sso_button_markup() { ?>
    <div id="google-sso-login" class="g-signin2" data-onsuccess="onSsoSuccess" onclick="javascript:markClicked()"></div>
<?php }

function selesti_sso_login()
{
    $response = array('success' => false, 'message' => 'an unknown error occured.', 'exception' => true );

    $sso_token = isset( $_POST['sso_token'] ) ? $_POST['sso_token'] : null;

    if( $sso_token ){

        $google_api_response = @file_get_contents('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='.$sso_token);

        if( $google_api_response ){

            try {
                $user = json_decode($google_api_response);

                if( $user->email_verified ){

                    if( isset($user->hd) ){

                        $login_key_pairs = array('selesti.com' => 'selesti'); //organisation => username

                        if( array_key_exists($user->hd, $login_key_pairs) ){
                            $username = $login_key_pairs[$user->hd];
                            $user_obj  = get_user_by('login', $username);

                            if( !$user_obj ){
                                $response['message'] = 'Couldnt find username '.$username;
                            }
                            else {
                                wp_set_auth_cookie($user_obj->ID, true);
                                $response['message']   = 'Logged in okay as '.$username;
                                $response['success']   = true;
                                $response['exception'] = false;
                            }
                        }
                        else {
                            $response['message'] = 'Your organisation hasnt been approved access to this system.';
                        }
                    }
                    else {
                        $response['message'] = 'You can only login with organisational accounts.';
                    }
                }
                else {
                    $response['message'] = 'You can only login with verified email accounts';
                }
            }
            catch(Exception $e){
                $response['message'] = 'Couldnt read JSON returned by Google SSO Server';
                $response['exception'] = $e;
            }

        }
        else {
            $response['message'] = 'Google SSO Server Rejected the Login Token';
        }

    }
    else {
        $response['message'] = 'SSO Provided No Login Token';
    }

    header('Content-type: application/json');
    exit(json_encode($response));
}

add_action( 'wp_ajax_selesti_sso_login', 'selesti_sso_login' );
add_action( 'wp_ajax_nopriv_selesti_sso_login', 'selesti_sso_login' );
add_action( 'login_enqueue_scripts', 'selesti_sso_markup' );
add_action( 'login_footer', 'selesti_sso_button_markup' );
